﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour {
    
    //0 stands for red, 1 for blue
    [SerializeField] public int inputValue;

    //objects references
    [SerializeField] HUDManager hud;
    [SerializeField] Robot robot;
    [SerializeField] Hard hard;

    //flags
    [SerializeField] public bool didInput = false;

    public bool canInput = true;

    public void RedInput()
    {
        if (canInput)
        {
            canInput = false;
            didInput = true;
            inputValue = 0;
            StartCoroutine(hud.CheckValues());
        }   
    }

    public void BlueInput()
    {
        if (canInput)
        {
            canInput = false;
            didInput = true;
            inputValue = 1;
            StartCoroutine(hud.CheckValues());
        }
    }
}
