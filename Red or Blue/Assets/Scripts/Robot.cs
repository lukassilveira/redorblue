﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Robot : MonoBehaviour {
    //objects references
    public Player player;
    public HUDManager hud;
    [Space]

    //data
    public int value;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private SpriteRenderer sprite;
    [Range(0, 5)] [SerializeField] private float height;
    [Range(0, 5)] [SerializeField] private float width;
    [Range(1, 50)] [SerializeField] public float speed;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip laughClip;
    [Space]

    //flags
    public bool isRandomizing;
    public bool canMove;
    public bool hasTricked;
    [Space]

    //timers
    //factor when robot is changing colors
    [Range(0.1f, 1)] [SerializeField] public float randomizeColorTimeFactor;
    [Space]

    [SerializeField] private float randomTime;
    [Range(0.1f, 1)] [SerializeField] public float minTimeToReady;
    [Range(0.1f, 3)] [SerializeField] public float maxTimeToReady;
    [Space]

    private float normalTimer;
    [SerializeField] public float timeToAnswer = 0;
    [SerializeField] public float maxTimeToAnswer = 2f;

    private void Start()
    {
        Time.timeScale = 1;
        value = 2;
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(hud.StartGame());
    }

    private void Update()
    {
        Randomizing();
        Moving();
        hud.UpdateSlider();
        Restart();
        Countdown();
    }

    private void Countdown()
    {
        if (!isRandomizing)
        {
            if (hud.canSlider) timeToAnswer += Time.deltaTime;
            if (timeToAnswer > maxTimeToAnswer)
            {
                value = 4;
                hud.gameOverText = GameObject.Find("GameOverText2").GetComponent<Animator>();
                hud.GameOver();
            }
        }
    }

    private static void Restart()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(1);
        }
    }

    public IEnumerator RandomizeValue(float time)
    {
        hud.slider.value = maxTimeToAnswer;
        player.canInput = true;
        timeToAnswer = 0;
        value = 2;
        hasTricked = false;
        //sprite shifting color
        randomTime = Random.Range(minTimeToReady + .5f, maxTimeToReady);
        isRandomizing = true;
        while (randomTime < maxTimeToReady)
        {
            canMove = true;
            sprite.color = Color.red;
            yield return new WaitForSeconds(time);
            sprite.color = Color.blue;
            yield return new WaitForSeconds(time);
        }

        canMove = false;
        randomTime = 0;
        isRandomizing = false;
        hud.canSlider = true;

        //final value
        int randomValue = Random.Range(0, 2);
        if (randomValue == 0)
        {
            sprite.color = Color.red;
            value = 0;
        }

        else
        {
            sprite.color = Color.blue;
            value = 1;
        }

        PlayParticles();
        //
    }

    public IEnumerator TrickValue(float time)
    {
        randomTime = Random.Range(minTimeToReady + .5f, maxTimeToReady);
        player.canInput = true;
        timeToAnswer = 0;
        isRandomizing = true;
        hud.slider.value = maxTimeToAnswer;
        value = 2;
        Debug.Log("Tricking, before color swaping!");
        while (randomTime < maxTimeToReady)
        {
            canMove = true;
            sprite.color = Color.red;
            yield return new WaitForSeconds(time);
            sprite.color = Color.blue;
            yield return new WaitForSeconds(time);
        }
        Debug.Log("Tricking, after color swaping!");
        canMove = false;
        randomTime = 0;
        isRandomizing = false;
        hud.canSlider = true;
        Debug.Log("Tricking, before final color!");
        //final color
        int randomValue = Random.Range(0, 2);
        if (randomValue == 0)
        {
            sprite.color = Color.red;
        }

        else
        {
            sprite.color = Color.blue;
        }
        Debug.Log("Tricking, after final color!");
        hud.canSlider = false;


        //PLAY ANIMATION
        yield return new WaitForSeconds(1.5f);

        audioSource.PlayOneShot(laughClip);
        GetComponent<Animator>().SetTrigger("Trick");

        yield return new WaitForSeconds(1.5f);
        StartCoroutine(RandomizeValue(randomizeColorTimeFactor));
    }

    public void Randomizing()
    {
        if (isRandomizing)
        {
            randomTime += Time.deltaTime;
        }
    }

    public void Moving()
    {
        Vector2 offset = new Vector2(0, 1);
        if (canMove)
        {
            normalTimer += Time.deltaTime*speed;

            float x = Mathf.Cos(normalTimer);
            float y = Mathf.Sin(normalTimer);

            transform.position = new Vector2(x*width, y*height) + offset;
        }
    }

    public void PlayParticles()
    {
        particle.startColor = sprite.color;
        particle.Play();
    }
}
