﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoundToggle : MonoBehaviour
{
    void Awake()
    {
        if (FindObjectOfType<AudioObject>().canPlay)
        {
            GetComponent<Image>().color = Color.white;
        }
        else
        {
            GetComponent<Image>().color = new Color(1, 1, 1, 0.15f);
        }
    }

    private void Update()
    {
    }
}
