﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Hard : MonoBehaviour
{
    public Player player;
    public HUDManager hud;
    [SerializeField] public int value;

    public enum Choose { Color, Text }
    public Choose choose;

    [Range(0, 5)] [SerializeField] private float height;
    [Range(0, 5)] [SerializeField] private float width;
    [Range(1, 50)] [SerializeField] public float speed;

    //flags
    public bool isRandomizing;
    public bool canMove;

    [Range(0.1f, 1)] [SerializeField] public float randomizeColorTimeFactor;
    [Space]

    [SerializeField] private float randomTime;
    [Range(0f, 1f)] [SerializeField] public float minTimeToReady;
    [Range(0f, 3f)] [SerializeField] public float maxTimeToReady;
    [Space]

    private float normalTimer;
    [SerializeField] public float timeToAnswer = 0;
    [SerializeField] public float maxTimeToAnswer = 2f;

    void Start()
    {
        Time.timeScale = 1;
        StartCoroutine(hud.StartGame());
    }

    void Update()
    {
        Randomizing();
        Moving();
        //UpdateChooseText();
        hud.UpdateSlider();
        Countdown();
        if (Input.GetKeyDown(KeyCode.A))
        {
            StartCoroutine(RandomizeValue(.1f));
        }
    }

    private void UpdateChooseText()
    {
        if (hud) hud.chooseText.text = choose == Choose.Text ? "Choose: TEXT" : "Choose: COLOR";
    }

    public IEnumerator RandomizeValue(float time)
    {
        if (hud) hud.slider.value = maxTimeToAnswer;
        if (player) player.canInput = true;
        timeToAnswer = 0;
        value = 2;
        isRandomizing = true;

        int randomChoose = Random.Range(0, 2);
        if (randomChoose == 0)
        {
            choose = Choose.Color;
            if (hud) hud.chooseText.text = "Choose: COLOR";
        }
        else
        {
            choose = Choose.Text;
            if (hud) hud.chooseText.text = "Choose: TEXT";
        }
        yield return new WaitForSeconds(1f);

        randomTime = Random.Range(minTimeToReady + .5f, maxTimeToReady);
        while (randomTime < maxTimeToReady)
        {
            canMove = true;
            int colorChance = Random.Range(0, 2);
            if (colorChance == 0) GetComponentInChildren<TMP_Text>().color = Color.red;
            else GetComponentInChildren<TMP_Text>().color = Color.blue;
            GetComponentInChildren<TMP_Text>().text = "RED";
            yield return new WaitForSeconds(time);
            colorChance = Random.Range(0, 2);
            if (colorChance == 0) GetComponentInChildren<TMP_Text>().color = Color.red;
            else GetComponentInChildren<TMP_Text>().color = Color.blue;
            GetComponentInChildren<TMP_Text>().text = "BLUE";
            yield return new WaitForSeconds(time);
        }
        int textChance = Random.Range(0, 2);
        GetComponentInChildren<TMP_Text>().text = textChance == 0 ? "RED" : "BLUE";
        canMove = false;
        randomTime = 0;
        isRandomizing = false;
        if (hud) hud.canSlider = true;

        
        if (randomChoose == 0)
        {
            if (GetComponentInChildren<TMP_Text>().color == Color.red) value = 0;
            else if (GetComponentInChildren<TMP_Text>().color == Color.blue) value = 1;
        } else
        {
            if (GetComponentInChildren<TMP_Text>().text == "RED") value = 0;
            else if (GetComponentInChildren<TMP_Text>().text == "BLUE") value = 1;
        }
    }

    public void Moving()
    {
        if (canMove)
        {
            normalTimer += Time.deltaTime * speed;

            float x = Mathf.Cos(normalTimer);
            float y = Mathf.Sin(normalTimer);

            transform.position = new Vector2(x * width, y * height);
        }
    }

    public void Randomizing()
    {
        if (isRandomizing)
        {
            randomTime += Time.deltaTime;
        }
    }

    private void Countdown()
    {
        if (!isRandomizing)
        {
            if (hud.canSlider) timeToAnswer += Time.deltaTime;
            if (timeToAnswer > maxTimeToAnswer)
            {
                value = 4;
                hud.gameOverText = GameObject.Find("GameOverText2").GetComponent<Animator>();
                hud.GameOver();
            }
        }
    }
}
