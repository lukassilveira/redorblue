﻿
using UnityEngine;
using UnityEngine.EventSystems;

public class Clicker : MonoBehaviour, IPointerDownHandler
{
    public enum Color { Blue, Red };
    [SerializeField] private Player player;
    [SerializeField] public Color color;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (color == Color.Blue) player.BlueInput();

        else if (color == Color.Red)  player.RedInput();
        
    }
}
