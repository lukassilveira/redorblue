﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {
    //objects references
    [SerializeField] private Player player;
    [SerializeField] private Robot robot;
    [SerializeField] private Hard hard;
    [SerializeField] private Animator playButton;
    [SerializeField] private Animator normalButton;
    [SerializeField] private Animator hardButton;
    [SerializeField] private Animator gameOverImage;
    [SerializeField] public Animator gameOverText;
    [SerializeField] private Animator returnButton;
    [SerializeField] private Animator retryButton;
    [SerializeField] private Animator gameOverFade;
    [SerializeField] public Slider slider;
    [SerializeField] public AudioClip scoreClip;
    [SerializeField] public AudioObject audioObject;
    [SerializeField] public Toggle soundToggle;

    //data
    [SerializeField] private int score;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] public TMP_Text chooseText;
    [SerializeField] public bool canSlider = true;
    [SerializeField] public float waitDelay;
    [SerializeField] public int trickCap;
    [SerializeField] public float minTimeToAnswerCap;
    [SerializeField] private float minTimeToAnswerCapHard;

    private void Start()
    {
        audioObject = FindObjectOfType<AudioObject>();
        soundToggle = FindObjectOfType<Toggle>();
    }

    public IEnumerator CheckValues()
    {
        if (robot != null)
        {
            if (player.inputValue == robot.value)
            {
                canSlider = false;
                score++;
                UpdateScore();
                yield return new WaitForSeconds(waitDelay);
                if (score > trickCap)
                {
                    Debug.Log("Tricking!");
                    //StartCoroutine(robot.TrickValue(robot.randomizeColorTimeFactor));

                    int shouldTrick = Random.Range(0, 10);
                    if (shouldTrick == 0)
                    {
                        StartCoroutine(robot.TrickValue(robot.randomizeColorTimeFactor));
                        robot.hasTricked = true;
                    }
                }
                if (!robot.hasTricked) StartCoroutine(robot.RandomizeValue(robot.randomizeColorTimeFactor));
                robot.maxTimeToAnswer *= 0.80f;
                robot.maxTimeToAnswer = Mathf.Clamp(robot.maxTimeToAnswer, minTimeToAnswerCap, robot.maxTimeToAnswer);
            }

            else
            {
                if (robot.value == 0 || robot.value == 1)
                    gameOverText = GameObject.Find("GameOverText3").GetComponent<Animator>();
                else if (robot.value == 2)
                    gameOverText = GameObject.Find("GameOverText1").GetComponent<Animator>();
                GameOver();
            }
        }
        if (hard != null)
        {
            Debug.Log("Hard is working");
            if (player.inputValue == hard.value)
            {
                canSlider = false;
                score++;
                UpdateScore();
                yield return new WaitForSeconds(waitDelay);
                StartCoroutine(hard.RandomizeValue(hard.randomizeColorTimeFactor));
                hard.maxTimeToAnswer *= 0.80f;
                hard.maxTimeToAnswer = Mathf.Clamp(hard.maxTimeToAnswer, minTimeToAnswerCapHard, hard.maxTimeToAnswer);
            }
            else
            {
                if (hard.value == 0 || hard.value == 1)
                    gameOverText = GameObject.Find("GameOverText3").GetComponent<Animator>();
                else if (hard.value == 2)
                    gameOverText = GameObject.Find("GameOverText1").GetComponent<Animator>();
                GameOver();
            }
        }
    }

    public void UpdateScore()
    {
        if (audioObject.canPlay) AudioSource.PlayClipAtPoint(scoreClip, new Vector3(0, 0, 0), 1);
        if (robot) robot.speed *= 1.05f;
        if (hard) hard.speed *= 1.05f;
        scoreText.text = "Score: " + score.ToString();
    }

    public void UpdateSlider()
    {
        if (robot)
        {
            slider.maxValue = robot.maxTimeToAnswer;
            if (slider.isActiveAndEnabled && canSlider) slider.value = slider.maxValue - robot.timeToAnswer;
        }
        if (hard)
        {
            slider.maxValue = hard.maxTimeToAnswer;
            if (slider.isActiveAndEnabled && canSlider) slider.value = slider.maxValue - hard.timeToAnswer;
        }
    }

    public void Scored()
    {
        int text = Random.Range(0, 2);
    }

    public void ClickPlay()
    {
        playButton.SetTrigger("ClickedPlay");
        normalButton.SetTrigger("ClickedPlay");
        hardButton.SetTrigger("ClickedPlay");
    }

    public void ClickNormal(Animator imageAnimator)
    {
        StartCoroutine(LoadGameScene(imageAnimator, "Game"));
    }

    public void ClickHard(Animator imageAnimator)
    {
        StartCoroutine(LoadGameScene(imageAnimator, "Test"));
    }

    public void ClickRetry()
    {
        StartCoroutine(Retry());
    }

    public void ClickReturnToMainMenu()
    {
        StartCoroutine(Return());
    }

    public void GameOver()
    {
        gameOverText.SetTrigger("GameOver");
        returnButton.SetTrigger("GameOver");
        retryButton.SetTrigger("GameOver");
        gameOverImage.SetTrigger("GameOver");
        Time.timeScale = 0;
    }

    public void ButtonSound(Image button)
    {
        audioObject.canPlay = !audioObject.canPlay;
        if (audioObject.canPlay)
        {
            audioObject.GetComponent<AudioSource>().Play();
            button.color = Color.white;
        }
        else
        {
            audioObject.GetComponent<AudioSource>().Stop();
            button.color = new Color(1, 1, 1, 0.15f);
        }
    }

    public IEnumerator Retry()
    {
        Debug.Log("Clicked Retry!");
        gameOverFade.SetTrigger("GameOver");
        yield return new WaitForSecondsRealtime(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public IEnumerator Return()
    {
        Debug.Log("Clicked Retry!");
        gameOverFade.SetTrigger("GameOver");
        yield return new WaitForSecondsRealtime(1);
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public IEnumerator LoadGameScene(Animator animator, string sceneName)
    {
        Debug.ClearDeveloperConsole();
        animator.SetTrigger("ClickedPlay");
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene(sceneName);
    }

    public IEnumerator StartGame()
    {
        player.canInput = false;
        yield return new WaitForSeconds(1.5f);
        slider.gameObject.SetActive(true);
        if (robot) StartCoroutine(robot.RandomizeValue(robot.randomizeColorTimeFactor));
        if (hard) StartCoroutine(hard.RandomizeValue(hard.randomizeColorTimeFactor));
        player.canInput = true;
    }
}
