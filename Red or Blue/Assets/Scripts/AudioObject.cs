﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioObject : MonoBehaviour {

    private int count;
    public bool canPlay;

    void Awake()
    {
        count = FindObjectsOfType<AudioObject>().Length;
        if (count > 1)
        {
            Destroy(gameObject);
        }

        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }   
}
